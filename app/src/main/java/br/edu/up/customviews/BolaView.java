package br.edu.up.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.View;

public class BolaView extends View {

  private Bitmap bitmap;

  public BolaView(Context context) {
    super(context);
    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bola_284px);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawBitmap(bitmap, 0, 0, null);
  }
}
