package br.edu.up.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class Tela extends Activity implements SensorEventListener {

  private Bitmap bitmap;
  private SensorManager sm;
  private BolaView bolaView;
  float x, y, sensorX, sensorY;

  @Override
  public void onAccuracyChanged(Sensor sensor, int i) {
    //Não utilizado...
  }

  @Override
  public void onSensorChanged(SensorEvent sensorEvent) {
    try {
      Thread.sleep(10);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    sensorX = sensorEvent.values[0];
    sensorY = sensorEvent.values[1];
  }

  public class BolaView extends SurfaceView implements Runnable {

    SurfaceHolder holder;
    boolean isRodando = true;
    Thread thread = null;

    public BolaView(Context context) {
      super(context);
      holder = getHolder();
    }

    public void pause() {
      isRodando = false;
      while (true) {
        try {
          thread.join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        break;
      }
      thread = null;
    }

    public void resume() {
      isRodando = true;
      thread = new Thread(this);
      thread.start();
    }

    @Override
    public void run() {
      while (isRodando) {
        if (!holder.getSurface().isValid())
          continue;

        Canvas canvas = holder.lockCanvas();
        if (canvas != null) {
          canvas.drawColor(Color.WHITE);
          int startX = canvas.getWidth() / 2 - bitmap.getWidth() / 2;
          int startY = canvas.getHeight() / 2 - bitmap.getHeight() / 2;
          canvas.drawBitmap(bitmap, startX + sensorX * 100, startY + sensorY * 100, null);
          holder.unlockCanvasAndPost(canvas);
        }
      }
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

    if (sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size() > 0) {
      Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
      sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bola_284px);
    x = y = sensorX = sensorY = 0;
    bolaView = new BolaView(this);
    bolaView.resume();
    setContentView(bolaView);
  }

  @Override
  protected void onPause() {
    super.onPause();
    sm.unregisterListener(this);
  }
}